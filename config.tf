# [required] relative path to thumbnail URL
variable thumbnail_url {}

# [optional] datetime for deployment
variable schedule_start {
    default = ""
}

# [optional] datetime for takedown
variable schedule_stop {
    default = ""
}

# [optional] string for base directory (presents, promos, pr, etcetc)
variable page_base {
    default = "presents"
}

# [required] Used in card title
variable page_title {}

# [required] Slug for page (harg-nallin-sclopio-peepio)
variable page_slug {} 
